<?php
declare(strict_types = 1);

namespace Runroom\GildedRose;

class GildedRose
{

    /**
     * Stores items objects
     *
     * @var array<Item> $items
     */
    private $items = array();

    /**
     * GildedRose constructor
     * @param array<Item> $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function update_quality(): void
    {
        foreach ($this->items as $item) {
            $item->update();
        }
        return;
    }
}

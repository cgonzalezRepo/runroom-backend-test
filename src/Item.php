<?php
declare(strict_types = 1);

namespace Runroom\GildedRose;

class Item
{

    /**
     * Maximum quality
     *
     * @var int
     */
    const MAX_QUALITY = 50;

    /**
     * Minimum quality
     *
     * @var int
     */
    const MIN_QUALITY = 0;

    /**
     * Maximum sellIn
     *
     * @var int
     */
    const MAX_SELL_IN = 11;

    /**
     * Medium sellIn
     *
     * @var int
     */
    const MEDIUM_SELL_IN = 6;


    /**
     * Minimum sellIn
     *
     * @var int
     */
    const MIN_SELL_IN = 0;

    /**
     * Aged brie constant string
     *
     * @var string
     */
    const AGED_BRIE = 'Aged Brie';

    /**
     * Backstage constant string
     *
     * @var string
     */
    const BACKSTAGE = 'Backstage passes to a TAFKAL80ETC concert';

    /**
     * Sulfuras constant string
     *
     * @var string
     */
    const SULFURAS = 'Sulfuras, Hand of Ragnaros';

    /**
     * Item name
     *
     * @var string
     */
    public $name;

    /**
     * Item sellin
     *
     * @var int
     */
    public $sellIn;

    /**
     * Item quality
     *
     * @var int
     */
    public $quality;


    /**
     * Item constructor
     * @param string $name
     * @param int $sellIn
     * @param int $quality
     */
    public function __construct(string $name, int $sellIn, int $quality)
    {
        $this->name = $name;
        $this->sellIn = $sellIn;
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->name}, {$this->sellIn}, {$this->quality}";
    }

    public function update() : void
    {
        $this->updateQuality();
        $this->updateSellIn();
        return;
    }

    private function updateQuality() : void
    {
        $this->updateQualityIfAgedBrieAndBackStage();
        return;
    }

    private function updateSellIn() : void
    {
        $this->decreaseSellInIfNotSulfuras();
        $this->handleQualityIfSellInLessThanMin();
        return;
    }

    private function increaseQualityIfSellInLessThanMax() : void
    {
        if ($this->sellIn < self::MAX_SELL_IN) {
            $this->increaseQualityIfLessThanMax();
        }
        return;
    }

    private function increaseQualityIfSellInLessThanMedium() : void
    {
        if ($this->sellIn < self::MEDIUM_SELL_IN) {
            $this->increaseQualityIfLessThanMax();
        }
        return;
    }


    private function increaseQualityIfLessThanMax() : void
    {
        if ($this->quality < self::MAX_QUALITY) {
            $this->increaseQuality();
        }
        return;
    }

    private function increaseQualityOfBackStage() : void
    {
        if ($this->name == self::BACKSTAGE) {
            $this->increaseQualityIfSellInLessThanMax();
            $this->increaseQualityIfSellInLessThanMedium();
        }
        return;
    }

    private function handleAgedBrieQuality() : void
    {
        if ($this->name != self::AGED_BRIE) {
            $this->decreaseQualityIfNotBackstage();
            $this->expireQualityIfBackStage();
        } else {
            $this->increaseQualityIfLessThanMax();
        }
        return;
    }

    private function handleQualityIfSellInLessThanMin() : void
    {
        if ($this->sellIn < self::MIN_SELL_IN) {
            $this->handleAgedBrieQuality();
        }
        return;
    }

    private function updateQualityIfAgedBrieAndBackStage() : void
    {
        if ($this->name == self::AGED_BRIE || $this->name == self::BACKSTAGE) {
            $this->increaseQualityIfLessThanMax();
            $this->increaseQualityOfBackStage();
        } else {
            $this->decreaseQualityIfNotSulfuras();
        }
        return;
    }

    private function increaseQuality() : void
    {
        $this->quality++;
        return;
    }

    private function decreaseQualityIfNotBackstage() : void
    {
        if ($this->name != self::BACKSTAGE) {
            $this->decreaseQualityIfNotSulfuras();
        }
        return;
    }

    private function decreaseQualityIfNotSulfuras() : void
    {
        if ($this->name != self::SULFURAS) {
            $this->decreaseQualityIfGreaterThanMin();
        }
        return;
    }

    private function decreaseQualityIfGreaterThanMin() : void
    {
        if ($this->quality > self::MIN_QUALITY) {
            $this->decreaseQuality();
        }
        return;
    }

    private function decreaseQuality() : void
    {
        $this->quality--;
        return;
    }

    private function decreaseSellInIfNotSulfuras() : void
    {
        if ($this->name != self::SULFURAS) {
            $this->decreaseSellIn();
        }
    }

    private function decreaseSellIn() : void
    {
        $this->sellIn--;
        return;
    }

    private function expireQualityIfBackStage() : void
    {
        if ($this->name == self::BACKSTAGE) {
            $this->expireQuality();
        }
    }

    private function expireQuality() : void
    {
        $this->quality = 0;
        return;
    }
}
